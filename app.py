# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.
import os
import sgtk
from tank.util import sgre as re

logger = sgtk.platform.get_logger(__name__)


class MultiActions(sgtk.platform.Application):
    """
    This is the :class:`sgtk.platform.Application` subclass that defines the
    top-level tk-multi-actions interface.
    """

    def init_app(self):
        """
        Called as the application is being initialized
        """

        print("INIT APP Tk-Multi-Actions")
        self.logger.info("INIT APP Tk-Multi-Actions")

        tk_multi_actions = self.import_module("tk_multi_actions")

        # the manager class provides the interface for the actions. We store a
        # reference to it to enable the create_actions_manager method exposed on
        # the application itself
        self._manager_class = tk_multi_actions.ActionsManager

        self._manager = self.create_actions_manager()

        # make the base plugins available via the app
        self._base_hooks = tk_multi_actions.base_hooks

        self._manager.load_actions()
        for action in self._manager.actions.values():
            self.logger.info("register action: {}".format(action.name))
            menu_options = {
                "title": action.display_name,
                "short_name": action.name,
                "description": action.description,
                "icon": action.icon,
            }
            if action.group is not None:
                menu_options["group"] = action.group
            if action.group_default is not None:
                menu_options["group_default"] = action.group_default
            if action.deny_permissions is not None:
                menu_options["deny_permissions"] = action.deny_permissions
            if action.deny_platforms is not None:
                menu_options["deny_platforms"] = action.deny_platforms
            if action.support_multiple_selection is not None:
                menu_options["support_multiple_selection"] = action.support_multiple_selection
            if action.type is not None:
                menu_options["type"] = action.type

            self.engine.register_command(action.display_name, action.execute, menu_options)

    @property
    def base_hooks(self):
        """
        Exposes the tk-multi-actions ``base_hooks`` module.

        This module provides base class implementations of actions
        plugin hooks:

        - :class:`~.base_hooks.ActionPlugin`

        Access to these classes won't typically be needed when writing hooks as
        they are are injected into the class hierarchy automatically for any
        action plugins configured.

        :return: A handle on the app's ``base_hooks`` module.
        """
        return self._base_hooks

    @property
    def context_change_allowed(self):
        """
        Specifies that context changes are allowed.
        """
        return True

    def create_actions_manager(self):
        """
        Create and return a :class:`tk_multi_actions.ActionsManager` instance.
        See the :class:`tk_multi_actions.ActionsManager` docs for details on
        how it can be used to automate your actions.

        :returns: A :class:`tk_multi_actions.ActionsManager` instance
        """
        return self._manager_class()

    def destroy_app(self):
        """
        Tear down the app
        """
        self.log_debug("Destroying tk-multi-actions")
