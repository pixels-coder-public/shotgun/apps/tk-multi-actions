
import sgtk

__logger = sgtk.platform.get_logger(__name__)
class Action(object):

    def __init__(
            self, name, display_name, hook, settings={}, description=None, logger=None,
            group=None, type=None, group_default=None, deny_permissions=None, deny_platforms=None,
            support_multiple_selection=None):
        super(Action, self).__init__()
        self._name = name
        self._group = group
        self._group_default = group_default
        self._deny_permissions = deny_permissions
        self._deny_platforms = deny_platforms
        self._support_multiple_selection = support_multiple_selection
        self._type = type
        self._display_name = display_name
        self._hook = hook
        self._description = description
        self._settings = settings
        self._logger = logger
        self._hook_instance = None
    
    @property
    def name(self):
        return self._name
    
    @property
    def group(self):
        return self._group
    
    @property
    def group_default(self):
        return self._group_default
    
    @property
    def deny_permissions(self):
        return self._deny_permissions
    
    @property
    def deny_platforms(self):
        return self._deny_platforms
    
    @property
    def support_multiple_selection(self):
        return self._support_multiple_selection
    
    @property
    def type(self):
        return self._type
    
    @property
    def display_name(self):
        return self._display_name
    
    @property
    def description(self):
        return self._description
    
    @property
    def hook(self):
        return self._hook
    
    @property
    def settings(self):
        return self.settings
    
    @property
    def logger(self):
        if self._logger:
            return self._logger
        return __logger
    
    @property
    def icon(self):
        return self.hook_instance.icon
    
    @property
    def hook_instance(self):
        if self._hook_instance is None:
            bundle = sgtk.platform.current_bundle()
            self._hook_instance = bundle.create_hook_instance(
                self._hook, base_class=bundle.base_hooks.ActionBaseHook
            )
        return self._hook_instance
    
    def build_settings(self, config_settings):
        settings = {}
        schema = self.hook_instance.settings
        for key, key_schema in schema.items():
            if "default" in key_schema:
                settings[key] = key_schema["default"]
            if key in config_settings:
                settings[key] = config_settings[key]
            if key not in settings:
                raise Exception("Setting {} must be provided in configuration, or as default value.".format(key))
            v = settings[key]
            self.logger.info("1 key_schema: {}".format(key_schema))
            if key_schema["type"] == "bool":
                if not isinstance(v, bool):
                    raise ValueError("Setting {} require a boolean. Found {}".format(key, repr(v)))
            elif key_schema["type"] == "int":
                if not isinstance(v, int):
                    raise ValueError("Setting {} require an integer. Found {}".format(key, repr(v)))
            elif key_schema["type"] == "float":
                if not isinstance(v, float):
                    raise ValueError("Setting {} require a float. Found {}".format(key, repr(v)))
            elif key_schema["type"] == "str":
                if not isinstance(v, str):
                    raise ValueError("Setting {} require a string. Found {}".format(key, repr(v)))
            elif key_schema["type"] == "template":
                if not isinstance(v, str):
                    raise ValueError("Setting {} require a strimg. Found {}".format(key, repr(v)))
                if v not in sgtk.platform.current_engine().sgtk.templates:
                    raise ValueError("Setting {} require a template name. Found {}".format(key, repr(v)))
                settings[key] = sgtk.platform.current_engine().sgtk.templates[key]
            else:
                raise Exception("Invalid Setting type {}.".format(key_schema["type"]))
            
        for key, value in config_settings.items():
            if key in settings:
                continue
            raise Exception("Invalid Setting {} not present in settings schema.".format(key))
        return settings

    
    def execute(self):
        self.logger.info("Running execute of Action Hook {}".format(self.name))
        settings = self.build_settings(self._settings)
        self.hook_instance.execute(settings)
