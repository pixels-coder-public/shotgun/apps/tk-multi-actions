# Copyright (c) 2018 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import fnmatch
from collections import OrderedDict

import sgtk

from .action import Action

logger = sgtk.platform.get_logger(__name__)


class ActionsManager(object):
    """
    This class is used for managing and executing action.
    """

    CONFIG_ACTIONS_DEFINITIONS = "actions"

    ############################################################################
    # instance methods

    def __init__(self, actions_logger=None):
        """
        Initialize the manager.

        :param publish_logger: This is a standard python logger to use during
            actions. A default logger will be provided if not supplied. This
            can be useful when implementing a custom UI, for example, with a
            specialized log handler
        """

        # the current bundle (the publisher instance)
        self._bundle = sgtk.platform.current_bundle()

        # a logger to be used by the various collector/publish plugins
        self._logger = actions_logger or logger

        self._actions = OrderedDict()
    
    @property
    def actions(self):
        return self._actions
    
    @property
    def logger(self):
        return self._logger

    def load_actions(self, context=None):
        """
        Load the actions from the configuration.
        :returns: None, the list of the loaded action can be retrieved using self.actions
        """

        if context is None:
            context = self._bundle.context

        engine = self._bundle.engine

        if context == self._bundle.context:
            # if the context matches the bundle, we don't need to do any extra
            # work since the settings are already accessible
            logger.debug("Finding publish plugin settings for context: %s" % (context,))
            actions_settings = self._bundle.get_setting(self.CONFIG_ACTIONS_DEFINITIONS)
        else:
            # load the plugins from the supplied context. this means executing
            # the pick environment hook and reading from disk. this is why we
            # cache the plugins.
            logger.debug(
                "Finding publish plugin settings via pick_environment for context: %s"
                % (context,)
            )
            context_settings = sgtk.platform.engine.find_app_settings(
                engine.name,
                self._bundle.name,
                self._bundle.sgtk,
                context,
                engine_instance_name=engine.instance_name,
            )

            app_settings = None
            if len(context_settings) > 1:
                # There's more than one instance of that app for the engine
                # instance, so we'll need to deterministically pick one. We'll
                # pick the one with the same application instance name as the
                # current app instance.
                for settings in context_settings:
                    if settings.get("app_instance") == self._bundle.instance_name:
                        app_settings = settings.get("settings")
            elif len(context_settings) == 1:
                app_settings = context_settings[0].get("settings")

            if app_settings:
                actions_settings = app_settings[self.CONFIG_ACTIONS_DEFINITIONS]
            else:
                logger.debug(
                    "Could not find actions settings for context: %s"
                    % (context,)
                )
                actions_settings = []

        # create plugin instances for configured plugins
        for action_def in actions_settings:

            logger.debug("Found action config: %s" % (action_def,))
            action = Action(
                action_def["name"],
                action_def.get("display_name", action_def["name"]),
                action_def["hook"],
                settings=action_def.get("settings", {}),
                description=action_def.get("description", "No description"),
                logger=self.logger,
                group=action_def.get("group", None),
                type=action_def.get("type", None),
                group_default=action_def.get("group_default", None),
            )
            
            logger.debug("Created action: %s" % (action,))
            self._actions[action.name] = action
