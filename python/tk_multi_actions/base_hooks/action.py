
import sgtk

HookBaseClass = sgtk.get_hook_baseclass()

class ActionBaseHook(HookBaseClass):

    @property
    def icon(self):
        return None
    
    @property
    def settings(self):
        return {}
    
    def execute(self, settings):
        pass