# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint
import sgtk
from tank_vendor import six

HookBaseClass = sgtk.get_hook_baseclass()


class SimpleActionHook(HookBaseClass):
    """
    Plugin for sending quicktimes and images to shotgun for review.
    """

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(self.disk_location, "icons", "file.png")

    @property
    def settings(self):
        return {
            "test_setting": {
                "type": "str",
                "default": "default_value",
            }
        }
    
    def execute(self, settings):
        """
        Executes the action
        """
        self.logger.info("Execute Simple Action, settings: {}".format(settings))
